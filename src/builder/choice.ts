/** Dependencies */
import {
    Collection,
    Components,
    Forms,
    REGEX_IS_URL,
    Slots,
    affects,
    created,
    definition,
    deleted,
    editor,
    isBoolean,
    isString,
    name,
    pgettext,
    refreshed,
    renamed,
    reordered,
} from "tripetto";
import { PictureChoice } from "./";

export class Choice extends Collection.Item<PictureChoice> {
    @definition
    @name
    name = "";

    @definition
    nameVisible = true;

    @definition
    image?: string;

    @definition
    emoji?: string;

    @definition
    description?: string;

    @definition
    url?: string;

    @definition
    @affects("#refresh")
    moniker?: string;

    @definition
    @affects("#refresh")
    value?: string;

    @definition
    exclusive?: boolean;

    @created
    @reordered
    @renamed
    @refreshed
    defineSlot(): void {
        if (this.ref.multiple) {
            this.ref.slots.dynamic({
                type: Slots.Boolean,
                reference: this.id,
                label: pgettext("block:picture-choice", "Image"),
                sequence: this.index,
                name: this.moniker || this.name,
                alias: this.value,
                required: this.ref.required,
                exportable: this.ref.exportable,
                pipeable: {
                    group: "Image",
                    label: pgettext("block:picture-choice", "Image"),
                    template: "name",
                    alias: this.ref.alias,
                },
            });
        } else {
            this.deleteSlot();
        }
    }

    @deleted
    deleteSlot(): void {
        this.ref.slots.delete(this.id, "dynamic");
    }

    @editor
    defineEditor(): void {
        const image = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "image", undefined)
        )
            .label(pgettext("block:picture-choice", "Image source URL"))
            .inputMode("url")
            .placeholder("https://")
            .autoValidate((ref: Forms.Text) =>
                ref.value === ""
                    ? "unknown"
                    : REGEX_IS_URL.test(ref.value) ||
                      (ref.value.length > 23 &&
                          ref.value.indexOf("data:image/jpeg;base64,") === 0) ||
                      (ref.value.length > 22 &&
                          ref.value.indexOf("data:image/png;base64,") === 0) ||
                      (ref.value.length > 22 &&
                          ref.value.indexOf("data:image/svg;base64,") === 0) ||
                      (ref.value.length > 22 &&
                          ref.value.indexOf("data:image/gif;base64,") === 0) ||
                      (ref.value.length > 1 && ref.value.charAt(0) === "/")
                    ? "pass"
                    : "fail"
            )
            .visible(!isString(this.emoji));

        const emoji = new Forms.Group([
            new Forms.Text(
                "singleline",
                Forms.Text.bind(this, "emoji", undefined)
            )
                .label(pgettext("block:picture-choice", "Emoji"))
                .maxLength(12)
                .width(65)
                .align("center"),
            new Forms.Static(
                pgettext(
                    "block:picture-choice",
                    "Windows users press: **WIN + .**"
                )
            ).markdown(),
            new Forms.Static(
                pgettext(
                    "block:picture-choice",
                    "MacOS users press: **CTRL + CMD + Space**"
                )
            ).markdown(),
        ]).visible(isString(this.emoji));

        this.editor.option({
            name: pgettext("block:picture-choice", "Label"),
            form: {
                title: pgettext("block:picture-choice", "Image label"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .autoFocus()
                        .autoSelect(),
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Show this label with the image"
                        ),
                        Forms.Checkbox.bind(this, "nameVisible", true)
                    ),
                ],
            },
            locked: true,
        });

        this.editor.form({
            title: pgettext("block:picture-choice", "Image"),
            controls: [
                new Forms.Radiobutton<"image" | "emoji">(
                    [
                        {
                            label: pgettext(
                                "block:picture-choice",
                                "Use an image"
                            ),
                            value: "image",
                        },
                        {
                            label: pgettext(
                                "block:picture-choice",
                                "Use an Emoji"
                            ),
                            value: "emoji",
                        },
                    ],
                    isString(this.emoji) ? "emoji" : "image"
                ).on((type) => {
                    this.emoji =
                        type.value === "emoji" ? this.emoji || "" : undefined;

                    image.visible(type.value === "image");
                    emoji.visible(type.value === "emoji");
                }),
                image,
                emoji,
            ],
        });

        this.editor.option({
            name: pgettext("block:picture-choice", "Description"),
            form: {
                title: pgettext("block:picture-choice", "Image description"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "description", undefined)
                    ),
                ],
            },
            activated: isString(this.description),
        });

        this.editor.group(pgettext("block:picture-choice", "Options"));

        this.editor.option({
            name: pgettext("block:picture-choice", "URL"),
            form: {
                title: pgettext("block:picture-choice", "URL"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "url", undefined)
                    )
                        .placeholder("https://")
                        .autoValidate((url: Forms.Text) =>
                            url.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(url.value)
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Static(
                        pgettext(
                            "block:picture-choice",
                            // tslint:disable-next-line:max-line-length
                            "If a URL is set, clicking the image will open it. The image cannot be selected as answer."
                        )
                    ),
                ],
            },
            activated: isString(this.url),
            on: (urlFeature: Components.Feature<Forms.Form>) => {
                monikerFeature.disabled(urlFeature.isActivated);
                identifierFeature.disabled(urlFeature.isActivated);
                exclusivityFeature.disabled(
                    urlFeature.isActivated || !this.ref.multiple
                );
            },
        });

        const monikerFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Moniker"),
            form: {
                title: pgettext("block:picture-choice", "Image moniker"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "moniker", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:picture-choice",
                            "If a moniker is set, this moniker will be used when referred to this image."
                        )
                    ),
                ],
            },
            activated: isString(this.moniker),
            disabled: isString(this.url),
        });

        const exclusivityFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Exclusivity"),
            form: {
                title: pgettext("block:picture-choice", "Image exclusivity"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Unselect all other selected images when selected"
                        ),
                        Forms.Checkbox.bind(this, "exclusive", undefined, true)
                    ),
                ],
            },
            activated:
                (this.ref.multiple &&
                    !isString(this.url) &&
                    isBoolean(this.exclusive)) ||
                false,
            disabled: !this.ref.multiple || isString(this.url),
        });

        const identifierFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Identifier"),
            form: {
                title: pgettext("block:picture-choice", "Image identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:picture-choice",
                            "If an image identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
            disabled: isString(this.url),
        });
    }
}
