/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import { ConditionBlock, pgettext, tripetto } from "tripetto";
import { PictureChoice } from "..";

/** Assets */
import ICON from "../../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:undefined`,
    version: PACKAGE_VERSION,
    context: PictureChoice,
    icon: ICON,
    get label() {
        return pgettext("block:picture-choice", "No image selected");
    },
})
export class PictureChoiceUndefinedCondition extends ConditionBlock {}
