/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    affects,
    definition,
    pgettext,
    tripetto,
} from "tripetto";
import { PictureChoice } from "..";
import { Choice } from "../choice";

/** Assets */
import ICON from "../../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    alias: "picture-choice",
    context: PictureChoice,
    icon: ICON,
    get label() {
        return pgettext("block:picture-choice", "Image");
    },
})
export class PictureChoiceCondition extends ConditionBlock {
    @affects("#condition")
    @definition("choices")
    choice: Choice | undefined;

    get name() {
        return (this.choice ? this.choice.name : "") || this.type.label;
    }

    get label() {
        return this.node && this.node.block instanceof PictureChoice
            ? this.node.block.alias || this.node.label || ""
            : "";
    }
}
