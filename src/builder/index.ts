/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    NodeBlock,
    REGEX_IS_URL,
    Slots,
    affects,
    conditions,
    definition,
    editor,
    isBoolean,
    isString,
    npgettext,
    pgettext,
    slotInsertAction,
    slots,
    tripetto,
} from "tripetto";
import { Choice } from "./choice";
import { PictureChoiceCondition } from "./conditions/choice";
import { PictureChoiceUndefinedCondition } from "./conditions/undefined";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    alias: "picture-choice",
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:picture-choice", "Picture choice");
    },
})
export class PictureChoice extends NodeBlock {
    @definition
    caption?: string;

    @definition
    imageURL?: string;

    @definition
    imageWidth?: string;

    @definition
    imageAboveText?: boolean;

    @definition
    @affects("#name")
    choices = Collection.of(Choice, this as PictureChoice);

    @definition
    @affects("#slots")
    @affects("#collection", "choices")
    multiple?: boolean;

    @definition
    @affects("#required")
    @affects("#slots")
    @affects("#collection", "choices")
    required?: boolean;

    @definition
    @affects("#slots")
    @affects("#collection", "choices")
    @affects("#label")
    alias?: string;

    @definition
    @affects("#slots")
    @affects("#collection", "choices")
    exportable?: boolean;

    @definition
    size?: "small" | "medium" | "large";

    get label() {
        return npgettext(
            "block:picture-choice",
            "%2 (%1 image)",
            "%2 (%1 images)",
            this.choices.count,
            this.type.label
        );
    }

    @slots
    defineSlot(): void {
        if (this.multiple) {
            this.slots.delete("choice", "static");
        } else {
            this.slots.static({
                type: Slots.String,
                reference: "choice",
                label: pgettext("block:picture-choice", "Image"),
                alias: this.alias,
                required: this.required,
                exportable: this.exportable,
            });
        }
    }

    @editor
    defineEditor(): void {
        this.editor.name(true, true);
        this.editor.option({
            name: pgettext("block:picture-choice", "Caption"),
            form: {
                title: pgettext("block:picture-choice", "Caption"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "caption", undefined)
                    )
                        .placeholder(
                            pgettext(
                                "block:picture-choice",
                                "Type caption text here..."
                            )
                        )
                        .action("@", slotInsertAction(this)),
                ],
            },
            activated: isString(this.caption),
        });
        this.editor.description();
        this.editor.option({
            name: pgettext("block:picture-choice", "Image"),
            form: {
                title: pgettext("block:picture-choice", "Image"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "imageURL", undefined)
                    )
                        .label(
                            pgettext("block:picture-choice", "Image source URL")
                        )
                        .inputMode("url")
                        .placeholder("https://")
                        .autoValidate((ref: Forms.Text) =>
                            ref.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(ref.value) ||
                                  (ref.value.length > 23 &&
                                      ref.value.indexOf(
                                          "data:image/jpeg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/png;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/svg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/gif;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 1 &&
                                      ref.value.charAt(0) === "/")
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Text(
                        "singleline",
                        Forms.Checkbox.bind(this, "imageWidth", undefined)
                    )
                        .label(
                            pgettext(
                                "block:picture-choice",
                                "Image width (optional)"
                            )
                        )
                        .width(100)
                        .align("center"),
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Display image on top of the paragraph"
                        ),
                        Forms.Checkbox.bind(this, "imageAboveText", undefined)
                    ),
                ],
            },
            activated: isString(this.imageURL),
        });
        this.editor.explanation();
        this.editor.collection({
            collection: this.choices,
            title: pgettext("block:picture-choice", "Images"),
            placeholder: pgettext("block:picture-choice", "Unnamed image"),
            editable: true,
            sorting: "manual",
        });

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:picture-choice", "Multiple select"),
            form: {
                title: pgettext("block:picture-choice", "Multiple select"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Allow the selection of multiple images"
                        ),
                        Forms.Checkbox.bind(this, "multiple", undefined, true)
                    ),
                ],
            },
            activated: isBoolean(this.multiple),
        });
        this.editor.option({
            name: pgettext("block:picture-choice", "Size"),
            form: {
                title: pgettext("block:picture-choice", "Size"),
                controls: [
                    new Forms.Radiobutton<"small" | "medium" | "large">(
                        [
                            {
                                label: pgettext(
                                    "block:picture-choice",
                                    "Small"
                                ),
                                value: "small",
                            },
                            {
                                label: pgettext(
                                    "block:picture-choice",
                                    "Medium"
                                ),
                                value: "medium",
                            },
                            {
                                label: pgettext(
                                    "block:picture-choice",
                                    "Large"
                                ),
                                value: "large",
                            },
                        ],
                        Forms.Radiobutton.bind(
                            this,
                            "size",
                            undefined,
                            "medium"
                        )
                    ),
                ],
            },
            activated: isString(this.size),
        });

        this.editor.groups.options();
        this.editor.required(this);
        this.editor.visibility();
        this.editor.alias(this);
        this.editor.exportable(this);
    }

    @conditions
    defineConditions(): void {
        this.choices.each((choice: Choice) => {
            if (choice.name) {
                this.conditions.template({
                    condition: PictureChoiceCondition,
                    label: choice.name,
                    props: {
                        choice: choice,
                        slot: this.slots.select(
                            this.multiple ? choice.id : "choice"
                        ),
                    },
                });
            }
        });

        if (this.choices.count > 0) {
            this.conditions.template({
                condition: PictureChoiceUndefinedCondition,
            });
        }
    }
}
