import {
    NodeBlock,
    assert,
    filter,
    findFirst,
    map,
    processor,
    validator,
} from "tripetto-runner-foundation";
import { IPictureChoice } from "./interface";
import "./conditions/choice";

export abstract class PictureChoice extends NodeBlock<IPictureChoice> {
    get required(): boolean {
        return this.props.required || false;
    }

    @processor
    processExclusive(): void {
        if (this.props.multiple) {
            const choices = filter(
                map(this.props.choices, (choice) => ({
                    id: choice.id,
                    exclusive: choice.exclusive,
                    valueRef: assert(this.valueOf<boolean>(choice.id)),
                })),
                (choice) => choice.valueRef.value === true
            ).sort((a, b) => (b.valueRef.time || 0) - (a.valueRef.time || 0));
            const lastSelected = choices.length && choices[0];

            if (lastSelected) {
                choices.forEach((c) => {
                    if (
                        c.id !== lastSelected.id &&
                        (lastSelected.exclusive || c.exclusive)
                    ) {
                        c.valueRef.value = false;
                    }
                });
            }
        }
    }

    @validator
    validate(): boolean {
        if (this.props.required) {
            if (this.props.multiple) {
                return findFirst(
                    this.props.choices,
                    (choice) =>
                        assert(this.valueOf<boolean>(choice.id)).value === true
                )
                    ? true
                    : false;
            } else {
                return assert(this.valueOf<string>("choice")).hasValue;
            }
        }

        return true;
    }
}
