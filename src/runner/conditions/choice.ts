/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

import {
    ConditionBlock,
    condition,
    isBoolean,
    tripetto,
} from "tripetto-runner-foundation";
import { IChoiceCondition } from "../interface";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    alias: "picture-choice",
})
export class PictureChoiceCondition extends ConditionBlock<IChoiceCondition> {
    @condition
    isChosen(): boolean {
        const pictureChoiceSlot =
            this.valueOf<boolean>(this.props.choice) ||
            this.valueOf<string>("choice");

        if (pictureChoiceSlot) {
            if (isBoolean(pictureChoiceSlot.value)) {
                return pictureChoiceSlot.value;
            }

            return pictureChoiceSlot.reference === this.props.choice;
        }

        return false;
    }
}
