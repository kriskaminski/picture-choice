export interface IChoice {
    readonly id: string;
    readonly name: string;
    readonly nameVisible: boolean;
    readonly image?: string;
    readonly emoji?: string;
    readonly description?: string;
    readonly url?: string;
    readonly value?: string;
    readonly exclusive?: boolean;
}

export interface IPictureChoice {
    readonly caption?: string;
    readonly imageURL?: string;
    readonly imageWidth?: string;
    readonly imageAboveText?: boolean;
    readonly choices: IChoice[];
    readonly multiple?: boolean;
    readonly required?: boolean;
    readonly size?: "small" | "medium" | "large";
}

export interface IChoiceCondition {
    readonly choice: string | undefined;
}
